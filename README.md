# [deprecated] points-on-circle

Moved to codeberg: https://codeberg.org/evasync/points-on-circle

Given the number of points returns an array of xy coordinates on the circle's perimeter.
Optional: You can give the circle's radius and center point (x , y)

<img src="https://gitlab.com/pragalakis/points-on-circle/raw/master/visual.png" />

# Usage

[![NPM](https://nodei.co/npm/points-on-circle.png)](https://www.npmjs.com/package/points-on-circle)

#### `pointsOnCircle(NumberOfPoints, radius = 1, x = 0, y = 0)`

Where x and y is the center of the circle

```
let pointsOnCircle = require('points-on-circle')

let numPoints = 4
let rad = 10
let x = width/2
let y = height/2
let points = pointsOnCircle(numPoints ,rad, x, y)
console.log(points)

```

## License

MIT
